from api.courses.models import Course
from api.students.models import Students
from api.users.models import Users


def test_create_course(db):
    course = Course(
        name="Mathematics", max_students=30, num_credits=3, professor_name="John Smith"
    )
    with db() as session:
        session.add(course)
        session.commit()
        assert course.id is not None
        assert course.name == "Mathematics"
        assert course.max_students == 30
        assert course.num_credits == 3
        assert course.professor_name == "John Smith"

        # Test the many-to-many relationship with students
        assert len(course.students) == 0

        # Create a student and associate it with the course
        user = Users(
            password="test",
            email="test_unique@example.com",
            created_at="2021-01-01",
            updated_at="2021-01-01",
            first_name="John",
            last_name="Doe",
        )

        student = Students(
            user=user,
            enrollment_date="2021-01-01",
            min_course_credits=10,
        )
        course.students.append(student)
        session.commit()

        # Check if the student is associated with the course
        assert len(course.students) == 1
        assert course.students[0] == student
        assert student.courses[0] == course

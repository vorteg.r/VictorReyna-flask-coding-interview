from api.students.models import Students
from api.users.models import Users


def test_create_students(db):
    user = Users(
        password="test",
        email="test@example.com",
        created_at="2021-01-01",
        updated_at="2021-01-01",
        first_name="John",
        last_name="Doe",
    )

    student = Students(
        user=user,
        enrollment_date="2021-01-01",
        min_course_credits=10,
    )

    with db() as session:
        session.add(user)
        session.add(student)
        session.commit()

        # Verify that student data was created
        assert student.id == 2
        assert student.enrollment_date == "2021-01-01"
        assert student.min_course_credits == 10

        # Veriy relationship with User Table
        assert user.id == 2
        assert student.user_id == user.id
        assert student.user.password == "test"
        assert student.user.email == "test@example.com"

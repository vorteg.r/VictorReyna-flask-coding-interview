from sqlalchemy import ForeignKey, Integer
from sqlalchemy.orm import relationship
from sqlalchemy.orm import Mapped, mapped_column
from api.db import Base

from api.courses.models import students_courses


class Students(Base):
    __tablename__ = "students"
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    user_id: Mapped[int] = mapped_column(ForeignKey("users.id"), nullable=False)
    enrollment_date: Mapped[str] = mapped_column(nullable=False)
    min_course_credits: Mapped[int] = mapped_column(Integer, nullable=False)

    user = relationship("Users", backref="student")
    courses = relationship(
        "Course", secondary=students_courses, back_populates="students"
    )

from typing import Optional

from pydantic import BaseModel, Field

from flask_openapi3 import APIBlueprint
from sqlalchemy import select

from database import db
from api.students.models import Students
from api.users.routes import UserSchema

students_app = APIBlueprint("students_app", __name__)


class StudentSchema(BaseModel):
    id: int
    user_id: int
    enrollment_date: str
    min_course_credits: int
    user: UserSchema
    courses: list

    class Config:
        orm_mode = True


class StudentList(BaseModel):
    students: list[StudentSchema]


@students_app.get("/students", responses={"200": StudentList})
def get_students():
    with db.session() as session:
        students = session.execute(select(Students)).scalars().all()
        student_schemas = [StudentSchema.from_orm(student) for student in students]
        return StudentList(students=student_schemas).dict()

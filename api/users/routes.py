from typing import Optional
from http import HTTPStatus

from pydantic import BaseModel, Field

from flask_openapi3 import APIBlueprint
from sqlalchemy import select

from database import db
from api.users.models import Users

users_app = APIBlueprint("users_app", __name__)


class UserSchema(BaseModel):
    id: int
    password: str
    email: str
    created_at: str
    updated_at: str
    last_login: Optional[str] = Field(..., nullable=False)
    first_name: str
    last_name: str

    class Config:
        orm_mode = True


class UserList(BaseModel):
    users: list[UserSchema]


class UserResponse(BaseModel):
    code: int = Field(0, description="Status Code")
    message: str = Field("ok", description="Exception Information")
    data: Optional[UserSchema]


class UserPath(BaseModel):
    user_id: int = Field(..., description="user id")


@users_app.get("/users", responses={"200": UserList})
def get_users():
    with db.session() as session:
        users = session.execute(select(Users)).scalars().all()
        user_schemas = [UserSchema.from_orm(user) for user in users]
        return UserList(users=user_schemas).dict()


@users_app.get("/users/<int:id>", responses={"200": UserSchema})
def get_user():
    with db.session() as session:
        user = session.execute(select(Users)).filter_by(id=1)
        user_schemas = UserSchema.from_orm(user)
        return user_schemas.dict()


@users_app.post("/users", tags=None, responses={"200": UserResponse})
def create_user(body: UserSchema):
    print(body)
    return {"code": 0, "message": "ok"}, HTTPStatus.OK


@users_app.put("/users/<int:id>", tags=None)
def update_book(path: UserPath, body: UserSchema):
    print(path)
    print(body)
    return {"code": 0, "message": "ok"}


@users_app.delete("/users/<int:id>", tags=None, doc_ui=False)
def delete_book(path: UserPath):
    print(path)
    return {"code": 0, "message": "ok"}

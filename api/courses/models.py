from sqlalchemy import Column, Integer, String, Table, ForeignKey
from sqlalchemy.orm import relationship

from api.db import Base

students_courses = Table(
    "students_courses",
    Base.metadata,
    Column("student_id", Integer, ForeignKey("students.id")),
    Column("course_id", Integer, ForeignKey("courses.id")),
)


class Course(Base):
    __tablename__ = "courses"

    id = Column(Integer, primary_key=True)
    name = Column(String(100), nullable=False)
    max_students = Column(Integer, nullable=False)
    num_credits = Column(Integer, nullable=False)
    professor_name = Column(String(100), nullable=False)

    students = relationship(
        "Students", secondary=students_courses, back_populates="courses"
    )
